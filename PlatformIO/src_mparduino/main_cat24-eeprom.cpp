#if 0

/*
  Read and write settings and calibration data to an external I2C EEPROM
  By: Nathan Seidle
  SparkFun Electronics
  Date: December 11th, 2019
  License: This code is public domain but you buy me a beer if you use this 
  and we meet someday (Beerware license).
  Feel like supporting our work? Buy a board from SparkFun!
  https://www.sparkfun.com/products/14764

  This example demonstrates how to set the various settings for a given EEPROM.
  Read the datasheet! Each EEPROM will have specific values for:
  Overall EEPROM size in bytes (512kbit = 65536, 256kbit = 32768)
  Bytes per page write (64 and 128 are common)
  Whether write polling is supported
  
  The I2C EEPROM should have all its ADR pins set to GND (0). This is default
  on the Qwiic board.

  Hardware Connections:
  Plug the SparkFun Qwiic EEPROM to an Uno, Artemis, or other Qwiic equipped board
  Load this sketch
  Open output window at 115200bps
*/

#include <Arduino.h>

#ifdef __SAMD21G18A__
#define Serial SerialUSB
#else
#define Serial Serial
#endif

#include <Wire.h>

#include "SparkFun_External_EEPROM.h" // Click here to get the library: http://librarymanager/All#SparkFun_External_EEPROM
ExternalEEPROM myMem;


void setup()
{
  Serial.begin(115200);
  while (!Serial)
    delay(10); // will pause Zero, Leonardo, etc until serial console opens
  
  Serial.println(__FILE__);

  Wire.begin();
  //Wire.setClock(400000); //Most EEPROMs can run 400kHz and higher

  if (myMem.begin() == false)
  {
    Serial.println("No memory detected. HALT.");
    while (1);
  }
  Serial.println("Memory detected!");
  randomSeed(analogRead(0));

  //Set settings for this EEPROM
  myMem.setMemorySize(32 * 1024 / 8); //In bytes. 
  myMem.setPageSize(32); //In bytes. Has 128 byte page size.

  //myMem.enablePollForWriteComplete(); //Supports I2C polling of write completion (100ms delay between poll)
  myMem.setPageWriteTime(5); // max write time (5?)

  Serial.print("Mem size in bytes: ");
  Serial.println(myMem.length());

#if 0
  float myValue3 = -7.35;
  myMem.put(20, myValue3); //(location, data)
  float myRead3;
  myMem.get(20, myRead3); //location to read, thing to put data into
  Serial.print("I read: ");
  Serial.println(myRead3);
#endif

  uint16_t address = 0;
#if 0
  int32_t x[10];

  //  FILL AND WRITE 10 FLOATS
  for (int i = 0; i < 10; i++)
  {
    x[i] = (int32_t)(random(1000));
    Serial.print(x[i], HEX);
    Serial.print("\t");
    address = myMem.put(address, x[i]);
    //delay(100);   // wait after each write
  }
  Serial.println();
  //delay(1000);     // wait before reading

  //  CLEAR THE ARRAY
  for (int i = 0; i < 10; i++)
  {
    x[i] = 0;
    Serial.print(x[i], 5);
    Serial.print("\t");
  }
  Serial.println();

  //  READ BACK 10 FLOATS
  address = 0;
  for (int i = 0; i < 10; i++)
  {
    address = myMem.get(address, x[i]);
    //delay(10);     // wait after each read

    Serial.print(x[i],HEX);
    Serial.print("\t");
  }
#elif 1
  #define WRITE_LEN  250

  uint8_t buff[256];
  memset(buff, 1, 255);
  for (int i=0; i < 255; i++)
  {
      if (i < WRITE_LEN)
      {
        //buff[i] = random(0xFF);
        //buff[i] = 0xEF-i;
        buff[i] = i+0x10;
      }

      Serial.print((uint8_t)buff[i], HEX);
      Serial.print(", ");

  }
  Serial.println();
  Serial.println();

  address = 0;
  myMem.write(address, buff, WRITE_LEN);

  //delay(100);   // wait after each write
  memset(buff, 0, 255);

  address = 0;
  myMem.read(address, buff, WRITE_LEN);

  for (int i=0; i < 255; i++)
  {
      Serial.print(buff[i], HEX);
      Serial.print(", ");
  }

#endif
  Serial.println();
  Serial.println("done...");


}


void loop()
{
}

// -- END OF FILE --

#endif
