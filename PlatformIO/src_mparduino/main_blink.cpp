#if 1
#include <Arduino.h>

#define DELAY_TIME  2000
#define DO_LED

uint16_t  count = 0;

void setup()
{
  // initialize LED digital pin as an output.
#ifdef DO_LED
  pinMode(LED_BUILTIN, OUTPUT);
#endif

  Serial.begin(115200);
    while (!Serial)
      delay(10); // will pause Zero, Leonardo, etc until serial console opens

  delay(1000);
  Serial.println(".\n.\n** Setup Finished **");

  count = 0;
}

void loop()
{
 count++;

  Serial.println("Hi^");
  // turn the LED on (HIGH is the voltage level)
#ifdef DO_LED
  digitalWrite(LED_BUILTIN, HIGH);
#endif

  // wait for some time
  delay(DELAY_TIME);

  Serial.println("Lo_");
  // turn the LED off by making the voltage LOW
#ifdef DO_LED
  digitalWrite(LED_BUILTIN, LOW);
#endif
  delay(DELAY_TIME);
}
#endif