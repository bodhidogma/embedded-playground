/*
 * Blink
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */


#if 0
#include <Arduino.h>
#include <Servo.h>
#include <max6675.h>
#include <Wire.h>

int thermoDO = 4;
int thermoCS = 5;
int thermoCLK = 6;

MAX6675 thermocouple(thermoCLK, thermoCS, thermoDO);


// leonardo LED = 13
// thindev LED = 5

#define LED LED_BUILTIN

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position

void setup() {
  Serial.begin(115200);
  Serial.println("start");

  pinMode(LED, OUTPUT);
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object

  // wait for MAX chip to stabilize
  delay(500);
}

void loop() {

  for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position

  }
  digitalWrite(LED, HIGH);
  Serial.println("LED 1");
  Serial.print(thermocouple.readCelsius());
  Serial.println(" deg C");

  for (pos = 180; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position

  }
  digitalWrite(LED, LOW);
  Serial.println("LED 0");

  Serial.print(  thermocouple.readFahrenheit());
  Serial.println(" deg F");

  delay(500);

}

#endif