#if 0

/* Wire Master Read
 * by MX682X
 *
 * Demonstrates use of the New Wire library
 * Reads data from an I2C/TWI slave device
 * Refer to the "Wire Slave Write" example for use with this
 *
 * This example takes the input from Serial. If the serial input is 'm' or 'M',
 * this code requests 4 bytes from the slave with the address 0x54.
 * When using together with the complementary example, the slave sends it's millis() value.
 * This value is then sent to the serial monitor
 *
 * To use this, you need to connect the SCL and SDA pins of this device to the
 * SCL and SDA pins of a second device running the Wire Slave Write example.
 *
 * Pullup resistors must be connected between both data lines and Vcc.
 * See the Wire library README.md for more information.
 */

#include <Arduino.h>
#include <Wire.h>

#ifdef __SAMD21G18A__
#define Serial SerialUSB
#else
#define Serial Serial
#endif

#define I2C_DEVICE_ID 0x54

void rxDataWire(void);
void txDataWire(void);
void i2c_scan(void);

void receiveEvent(int bytes);
void requestEvent(void);

int8_t rxLen = 0;
int8_t len = 0;

void setup() {
  digitalWrite(LED_BUILTIN, HIGH);

#if 1
  Wire.begin();                                 // initialize master
#else
  Wire.begin(I2C_DEVICE_ID);
  Wire.onReceive(receiveEvent);
  //Wire.onRequest(requestEvent);
#endif
  Serial.begin(115200);

  delay(1000);

  digitalWrite(LED_BUILTIN, LOW);
  Serial.println("start...");

}

void loop() {
  if (Serial.available() > 0) {    // as soon as the first byte is received on Serial
    char c = Serial.read();       // read the data from serial.
    if (c == 't' || c == 'T') {
      txDataWire();               // send the data over I2C
    }
    else if (c == 'r' || c == 'r') {
      rxDataWire();               // send the data over I2C
    }
    else if (c == 's' || c == 'S') {
      i2c_scan();
    }
    else {
      Serial.print(c);
    }
    len = 0;                        // since the data was sent, the position is 0 again
  }
}

int x = 0;

void receiveEvent(int bytes)
{
  x = Wire.read();
  Serial.print(x);
}

void rxDataWire()
{
  char c;
  if (4 == Wire.requestFrom(I2C_DEVICE_ID, 4, 0x01)) {    // request from slave
    while (Wire.available()) {
      c = (char)Wire.read();
      Serial.print(c); Serial.print(" ");
      //ms  = (uint32_t)Wire.read();               // read out 32-bit wide data
      //ms |= (uint32_t)Wire.read() <<  8;
      //ms |= (uint32_t)Wire.read() << 16;
      //ms |= (uint32_t)Wire.read() << 24;
      //Serial.println(ms);              // print the milliseconds from Slave
    }
  } else {
    Serial.println("Wire.requestFrom() timed out!");
  }
}

void txDataWire()
{
  Wire.beginTransmission(I2C_DEVICE_ID); // transmit to device #9
  Wire.write(x);              // sends x 
  Wire.endTransmission();    // stop transmitting
  x++; // Increment x
  if (x > 5) x = 0; // `reset x once it gets 6
  Serial.print("t ");
}

void i2c_scan(void)
{
  byte error, address; //variable for error and I2C address
  int nDevices;

  Serial.println("Scanning...");

  nDevices = 0;
  for (address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println("  !");
      nDevices++;
    }
    else if (error == 4)
    {
      Serial.print("Unknown error at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");
}

#endif
