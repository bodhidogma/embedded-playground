/*
MODBUS / RS485 testing
 */

#if 0

#include <Arduino.h>

#if defined(__SAMD21G18A__)
#define Serial SerialUSB

#define Serial485 Serial1
#define RS485_RTS 2
// #define MASTER_TX 1
// #define MASTER_RX 0

#elif defined(ESP32)

#define Serial485 Serial2 // RS485 serial port
#define RS485_RTS 4       // RS485 en/rts

// #define MASTER_TX 17
// #define MASTER_RX 16

#else // other platforms

#endif

char sBuff[128];
uint8_t trxBuff[20];
uint8_t *pBuff;
uint8_t trxMsgLen;

#define DEF_MSGLEN 6

uint16_t crc_chk_value(char *data_value, size_t length);

void setup()
{
    digitalWrite(LED_BUILTIN, HIGH);

    Serial.begin(115200);
    Serial485.begin(9600);
    pinMode(RS485_RTS, OUTPUT);

    delay(1000);

    digitalWrite(LED_BUILTIN, LOW);
    Serial.println("start...");

    pBuff = trxBuff;
    trxMsgLen = 8;
}

/*
    https://drive.google.com/file/d/1VS2JYvjreUWsw3MOgpfzmrMsCzUD_mnA/view

    DEV ID | Function | ADDR/LEN | DATA | CRC
     1B    |  1B      |  2B      |  nB  |  2B

    F: x03 = read address
    F: x06 = set address data
    F: x83 = read instruction error return
    F: x86 = set specified address error return

    * read button status
    ->: 02 03 10 0B 01 <CRC>
    <-: 02 03 00 02 xx xx <CRC> (xx xx == button pressed)

    ---
    sequence
    setup()
        set panel config: slave-send
        > 02 06 1003 0004

        set panel config: 15s light off delay?
        > 02 06 1003 xxxx

    loop()
        if (rx-button-msg) > 02 03 0002 XXXX
            set-led for pressed button > 02 06 1008 XXXX
            poll to check for release > 02 03 131? 0001

*/

typedef struct
{
    // message length
    uint16_t len;
    // actual message data
    uint8_t id;
    uint8_t cmd;
    uint8_t addr[2];
    uint8_t data[18];
} PanelMessage;

enum PanelState
{
    RX_BUTTON,
    TX_STATUS,
    MAX_STATE
};

int txMessage(PanelMessage *msg)
{
    uint16_t crc_val = 0;
    uint8_t ret = 0;

    crc_val = crc_chk_value((char *)(&msg->id), msg->len - 2);
    msg->data[msg->len - DEF_MSGLEN] = (crc_val & 0xFF);
    msg->data[msg->len - DEF_MSGLEN + 1] = (crc_val & 0xFF00) >> 8;

    sprintf(sBuff, " > (%d) id: %2x cmd: %2x aln: %02x%02x dat: %02x%02x ", msg->len, msg->id, msg->cmd, 
        msg->addr[1], msg->addr[0], msg->data[1], msg->data[0]);
    Serial.print(sBuff);
    sprintf(sBuff, "(c:%04x)", crc_val);
    Serial.println(sBuff);

    digitalWrite(RS485_RTS, HIGH); // enable sending
    // delay(1);

    ret = Serial485.write((const char *)(&msg->id), msg->len); // send our cmd message
    Serial485.flush();                                         // wait for send to complete

    for (int i = 0; i < 2; i++) // wait a bit to ensure all bytes went out
        delay(1);
    digitalWrite(RS485_RTS, LOW); // enable reading

    return ret;
}

int rxMessage(PanelMessage *msg)
{
    uint8_t ret = 0;
#if 1
    uint16_t crc_val = 0;
    uint16_t crc_msg = 0;
    uint16_t msg_addr = 0;
    uint16_t msg_data = 0;

    crc_val = crc_chk_value((char *)(&msg->id), msg->len - 2);
    crc_msg = msg->data[msg->len - DEF_MSGLEN] | (msg->data[msg->len - DEF_MSGLEN + 1] << 8);
    msg_addr = msg->addr[1] | (msg->addr[0] << 8);
    // msg_data = msg->data[1] | (msg->data[0] << 8);

#if 1
    sprintf(sBuff, " > (%d) id: %2x cmd: %2x aln: %04x dat: %02x%02x ", msg->len, msg->id, msg->cmd, msg_addr, msg->data[0], msg->data[1]);
    Serial.println(sBuff);
#endif

    if (crc_val != crc_msg)
    {
        sprintf(sBuff, "(%04x %c= %04x)", crc_val, ((crc_val == crc_msg) ? ' ' : '!'), crc_msg);
        Serial.print(sBuff);
        // do something if CRC !=
    }

#if 0
    switch (msg->cmd)
    {
    case 0x3: // read address data
        switch (msg_addr)
        {
        case 0x2:
            uint8_t btn = 0;
            btn = msg->data[0] - ((msg_addr - 1) * 6);
            sprintf(sBuff, "btn=%d\n", btn);
            Serial.print(sBuff);
            break;
        }
        break;
    case 0x6:  // set address data
    case 0x83: // read instruction error
    case 0x86: // set address error
        break;
    }
#endif
#endif
    return ret;
}

void do_key1();
void do_key2();
void do_key3();

void loop()
{
    uint8_t ret = 0;
    //    uint16_t crc_val = 0;
    //    char tx_buff[16];
    PanelMessage msg = {0};
    uint16_t len = 0;
    // int len;

    digitalWrite(RS485_RTS, LOW);
    if (Serial.available() > 0)
    {                           // as soon as the first byte is received on Serial
        char c = Serial.read(); // read the data from serial.

        if (c == '1')
            do_key1();
        else if (c == '2')
            do_key2();
        else if (c == '3')
            do_key3();
        else
        {
            Serial.print(c);
        }
    }
    if (Serial485.available() > 0)
    { // as soon as the first byte is received on Serial
        // Serial.println("~");
        while (Serial485.available())
        {
            char c = Serial485.read(); // read the data from serial.

            sprintf(sBuff, ".%02x", c);
            Serial.print(sBuff);
            // Serial.print(c, HEX);
#if 1
            *pBuff++ = c;
            // check expected packet length
            if ((pBuff - trxBuff) == 4)
            {
                len = (trxBuff[2] << 8) | (trxBuff[3]);
                trxMsgLen = DEF_MSGLEN + len;
                sprintf(sBuff, "(%d)", len);
                Serial.print(sBuff);
            }
            // check for packet complete
            if (((pBuff - trxBuff) > 3) &&        // include packet length
                ((pBuff - trxBuff) < 20) &&       // not past buffer size
                ((pBuff - trxBuff) >= trxMsgLen)) // expected packet length
            {
                *pBuff = 0;
                msg.len = trxMsgLen;
                memcpy(&msg.id, trxBuff, trxMsgLen);
                rxMessage(&msg);
#if 0
                Serial.print(" : ");
                for (c = 0; c < 8; c++)
                    Serial.printf("%02x", trxBuff[c]);
#endif
                pBuff = trxBuff;
            }
#endif
            /*
            02:i
            03:c
            000c:d
            000200000000000000000001
            ac9c
            */
        }
    }
}

uint16_t val_1008 = 0;
uint16_t val_1003 = 0;

void do_key1()
{
    PanelMessage msg = {0};
    uint8_t ret = 0;

    msg.len = 2 + DEF_MSGLEN;
    msg.id = 0x02;
    msg.cmd = 0x03;                  // read
    memcpy(msg.addr, "\x13\x10", 2); // 1003 = cfg, 1008 = LED, 100B = BTN, 1310 = btn status
    memcpy(msg.data, "\x00\x06", 2); // # of btn registers to read

    ret = txMessage(&msg);
    if (ret != 8)
    {
        Serial.print("write-err: ");
        Serial.println(ret);
    }
}

void do_key2()
{
    PanelMessage msg = {0};
    uint8_t ret = 0;

    msg.len = 2 + DEF_MSGLEN;
    msg.id = 0x02;
    msg.cmd = 0x06;
    memcpy(msg.addr, "\x10\x08", 2); // LED ring B0-5 = D0-5 LED
    // val_1008 = 0;                    // all LED off
    // val_1008 = 0xFF;                 // state LED (all) on
    // val_1008 = 0x01FF;               // state + GND LED on
    // val_1008 = 0x0003;               // D1-2 LED on /others off
    // val_1008 = 0x0100;               // GND LED on / others off
    msg.data[0] = (val_1008 & 0x0100) >> 8;
    msg.data[1] = val_1008 & 0x00FF;

    ret = txMessage(&msg);
    if (ret != 8)
    {
        Serial.print("write-err: ");
        Serial.println(ret);
    }

    Serial.println(val_1008, HEX);
    if (!val_1008)
        val_1008 = 1;
    else
        val_1008 = val_1008 << 1;
    if (val_1008 > 0x0100)
        val_1008 = 0;
}

void do_key3()
{
    PanelMessage msg = {0};
    uint8_t ret = 0;

    msg.len = 2 + DEF_MSGLEN;
    msg.id = 0x02;
    msg.cmd = 0x06;
    memcpy(msg.addr, "\x10\x03", 2); // Panel config
    // memcpy(msg.data, "\x00\x00", 2);    // bit0=led anti, bit2=slave send mode, bit3=IR, bit4=led delay
    msg.data[0] = val_1003 & 0x0100;
    msg.data[1] = val_1003 & 0x00FF;

    ret = txMessage(&msg);
    if (ret != 8)
    {
        Serial.print("write-err: ");
        Serial.println(ret);
    }

    Serial.println(val_1003, HEX);
    if (!val_1003)
        val_1003 = 1;
    else
        val_1003 = val_1003 << 1;
    if (val_1003 > 0x80)
        val_1003 = 0;
}

int rxMessage(int bytes)
{
    return 0;
}

// https://gist.github.com/deldrid1/3839697
// https://github.com/LacobusVentura/MODBUS-CRC16
// CRC16-modbus
uint16_t crc_chk_value(char *data_value, size_t length)
{
    uint16_t crc_value = 0xFFFF;
    int i;
    while (length--)
    {
        crc_value ^= *data_value++;
        for (i = 0; i < 8; i++)
        {
            if (crc_value & 0x0001)
            {
                crc_value = (crc_value >> 1) ^ 0xa001;
            }
            else
            {
                crc_value = crc_value >> 1;
            }
        }
    }
    return (crc_value);
}

#endif
