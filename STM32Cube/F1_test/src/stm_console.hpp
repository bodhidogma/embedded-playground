/*
 */

#ifndef __STM_CONSOLE_HPP__
#define __STM_CONSOLE_HPP__

#include "main.h"

#include <stdbool.h>
#include <stdint.h>

const static uint8_t kConsoleBuffLen = 64;
static char console_buffer_[kConsoleBuffLen];
static uint8_t buffer_pos_;

const static uint8_t kUARTBuffLen = 16;
static uint8_t uart_buffer_[kUARTBuffLen];
static uint8_t uart_byte_;

const static uint8_t kCDCBuffLen = 16;
static uint8_t cdc_buffer_[kCDCBuffLen];
static uint8_t cdc_byte_;

class StmConsole {
 public:
  StmConsole(UART_HandleTypeDef *huart, bool cdc_uart);

  bool Initialize();
  bool Update();

 protected:
 private:
  bool rxEventCheck(void);

  UART_HandleTypeDef *my_huart_;
  bool my_cdc_uart_;
};

#endif