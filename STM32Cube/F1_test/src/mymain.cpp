/*
 */

#include "mymain.h"

#include "stm_console.hpp"

#include <stdio.h>

#if 0
#include <sys/unistd.h>  // STDOUT_FILENO, STDERR_FILENO
extern "C" {
int _write(int file, unsigned char *ptr, int len) {
  if ((file != STDOUT_FILENO) && (file != STDERR_FILENO)) {
    return -1;
  }
#if 0
    static uint8_t rc = USBD_OK;
    //do {
        rc = CDC_Transmit_FS(ptr, len);
    //} while (USBD_BUSY == rc);

    if (USBD_FAIL == rc) {
        /// NOTE: Should never reach here.
        /// TODO: Handle this error.
        return 0;
    }
    return len;
#else
  // arbitrary timeout 1000
  HAL_StatusTypeDef status =
      HAL_UART_Transmit(&huart1, (uint8_t *)ptr, len, 1000);

  // return # of bytes written - as best we can tell
  return (status == HAL_OK ? len : 0);
#endif
}
}
#endif

StmConsole console(&huart1, false);

static uint32_t sys_now_ms = 0;

void main_pre_loop(void) {

};

void main_loop(void) {
  uint32_t last_now_ms_ = 0;

  //uint8_t buffer[] = "Hello, World!\r\n";
  //CDC_Transmit_FS(buffer, sizeof(buffer));

  // disable stdio buffering
  setbuf(stdout, NULL);
  //console.Initialize();

  while (1) {
    sys_now_ms = millis();

#if 0
    HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
    HAL_Delay(1000);
    HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
    HAL_Delay(250);
    HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
    HAL_Delay(250);

    HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);

    printf("loop..\r\n");
#else

    if (sys_now_ms - last_now_ms_ > 1000) {
      last_now_ms_ = sys_now_ms;

      HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
      fprintf( stderr, ".");
      //printf(".");
    }

    //console.Update();
#endif
  }
}
