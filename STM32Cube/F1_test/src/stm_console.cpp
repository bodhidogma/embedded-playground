/*
*/

#include "stm_console.hpp"
#include "mymain.h"

#include <cstring>

/**
 * 
*/
StmConsole::StmConsole(UART_HandleTypeDef *huart, bool cdc_uart)
{
  my_huart_ = nullptr;
  my_cdc_uart_ = false;

  // define which uarts to use
  if (huart != nullptr) {
    my_huart_ = huart;
    // kick off UART rx DMA - trigger callback when RX data
    HAL_UART_Receive_IT(huart, &uart_buffer_[0], 4);
    //HAL_UART_Receive_DMA(&huart1, UART1_rxBuff, 1);
  }
  else {
    my_cdc_uart_ = cdc_uart;

  }
}

/** init console class
 * 
 * @return true
*/
bool StmConsole::Initialize(void) {
  memset(console_buffer_, 0, kConsoleBuffLen);
  buffer_pos_ = 0;
  return true;
}

/** Process console input and commands
 *
 * @return true if input was processed
 */
bool StmConsole::Update(void) {
  bool status = false;
  if (rxEventCheck()) {
    //ipc_command_.ProcessCmdBuff(console_buffer_, strlen(console_buffer_));
    buffer_pos_ = 0;
    status = true;
  }
  return status;
}

/** Check for RX events and append to buffer
 *
 * @return true when input is ready to process
 */
bool StmConsole::rxEventCheck(void) {
  bool cmd_ready = false;

  // return if no data to be read
  // if (!serial_.available()) return false;
#if 0
  while (serial_.available() && buffer_pos_ < kConsoleBuffLen) {
    console_buffer_[buffer_pos_] = serial_.read();
#if 1
    // CON_PRINTf("%d:%c", buffer_pos_, console_buffer_[buffer_pos_]);
    CON_PRINTf("%c", console_buffer_[buffer_pos_]);
#endif
    // null term end of our buffer
    console_buffer_[++buffer_pos_] = '\0';

    // check for end of buffer
    if (buffer_pos_ >= kConsoleBuffLen) {
      CON_PRINTf("<!>" NL);
      cmd_ready = true;
    }

    // check for end of line
    else if (console_buffer_[buffer_pos_ - 1] == '\r' ||
             console_buffer_[buffer_pos_ - 1] == '\n') {
      console_buffer_[--buffer_pos_] = '\0';
      // check for non-empty line / have some bytes
      if (buffer_pos_) {
        // CON_PRINTf("<%d>" NL, buffer_pos);
        cmd_ready = true;
      }
    }
    // misc chars to process
#if 1
    else if (console_buffer_[buffer_pos_ - 1] == '\b') {
      console_buffer_[--buffer_pos_] = '\0';
      if (buffer_pos_ > 0) {
        console_buffer_[--buffer_pos_] = '\0';
        CON_PRINT(" \b");
      }
    }
#endif
  }
#endif
  return cmd_ready;
}

/*
 * UART RX callback
 *

 */
extern "C"
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART1)
	{
  }
  //CmdBus.cmd_buff[cnt++] = UART1_rxBuff[0];

  {
    HAL_UART_Transmit(huart, uart_buffer_, 1, 100);
    //HAL_UART_Transmit_IT(huart, uart_buffer_, 1);
    //HAL_UART_Transmit_DMA(huart, uart_buffer_, 1);
  }
  //HAL_UART_Receive_IT(huart, &uart_buffer_, 1);
  //HAL_UART_Receive_DMA(huart, &uart_buffer_, 1);
}

