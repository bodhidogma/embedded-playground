#! /bin/sh


read -r -d '' INC_FILES <<-'EOF'
Core/Inc/*.h
USB_DEVICE/App/*.h
USB_DEVICE/Target/*.h
Middlewares/ST/STM32_USB_Device_Library/Core/Inc/*
Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc/*
EOF

read -r -d '' SRC_FILES <<-'EOF'
Core/Src/*.c
USB_DEVICE/App/*.c
USB_DEVICE/Target/*.c
Middlewares/ST/STM32_USB_Device_Library/Core/Src/*
Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Src/*
EOF

pushd ../..
#$ls $SRC_FILES | xargs -I % echo %
ls $INC_FILES | xargs -I % cp -v % "lib/cubemx_generated/Inc"
ls $SRC_FILES | xargs -I % cp -v % "lib/cubemx_generated/Src"

#echo "$DIST_LIST" | xargs -I % cp -r % "tmp-dist/${APP_DIR}"

# diff Src/main.c ../../Core/Src/main.c
# cp Src/main.c ../../Core/Src/

