
# CONNECTORS

* main / conn4 == usart1
  - 1 gnd (start from left - facing connector)
  - 2 vcc
  - 3 tx
  - 4 rx

* SWD / conn6 == debug
  - 1 gnd
  - 2 NRS
  - 3 io
  - 4 clk

# DEVICES

* I2C1
  - mss5611 (pressure)
  - hmc5883L (magnetometer) [PB7 == mag drdy]

* SPI1
  * mpu-6000 [PC4 == gyro int]

* SPI3
  * FLASH 16Mbit

* PC5 vbus sen
* PC2 (ADC123_12) volt / sonar
* PC1 (ADC123_11) curr
