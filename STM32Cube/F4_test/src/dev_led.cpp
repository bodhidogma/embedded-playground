/*
 */

#include "dev_led.hpp"

bool DevLED::Update() {
  uint32_t now_ms = millis();

  switch (pattern_) {
    case BLINK1:
      if (now_ms - last_now_ms_ > 1000) {
        last_now_ms_ = now_ms;
        HAL_GPIO_WritePin(port_, pin_, (GPIO_PinState)pat_step_);  //
        pat_step_ = !pat_step_;
      }
      break;

    case BLINK2:
    case BLINK3:
    case OFF:
    default:
      HAL_GPIO_WritePin(port_, pin_, GPIO_PIN_SET);  // RESET
      break;
  }

  return true;
}